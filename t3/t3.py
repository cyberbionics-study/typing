import sqlite3
import smtplib
import datetime

from typing import Any
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


class User:
    """
    class User is bonded to sqlite table 'Users', this bond is initialized when first class instance is created
    class instance should have first name, middle name, last name, birth date in datetime.date format
    (could be upgraded to take string and covert it to proper format) and email as a string.
    when initialized class instance make its record in sqlite table

    requested by task methods of getting full and short name, and age are realized by property
    """

    connection = sqlite3.connect('../data/UsersDB')
    cursor = connection.cursor()

    cursor.execute('''CREATE TABLE IF NOT EXISTS Users (
                      id INTEGER PRIMARY KEY,
                      last_name TEXT,
                      first_name TEXT,
                      email TEXT)''')

    def __init__(self, first_name: str, middle_name: str, last_name: str, birthday: datetime.date, email: str) -> None:
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name
        self.birthday = birthday
        self.email = email

        connection = sqlite3.connect('../data/UsersDB')
        cursor = connection.cursor()
        cursor.execute("INSERT INTO Users (last_name, first_name, email) VALUES (?, ?, ?)",
                   (self.last_name, self.first_name, self.email))
        connection.commit()

    def __str__(self):
        return f'{self.full_name}, {self.age} years'

    @property
    def full_name(self) -> str:
        return ' '.join(name for name in [self.last_name, self.first_name, self.middle_name])

    @property
    def short_name(self) -> str:
        res = [
            self.last_name,
            self.first_name[0] + '.',
            self.middle_name[0] + '.'
        ]
        return ' '.join(elem for elem in res)

    @property
    def age(self) -> int:
        res = datetime.date.today() - self.birthday
        return int(res.days / 365.25)


def search_user_in_db(first_name: str, last_name: str, email: str) -> Any:
    """this just works"""
    connection = sqlite3.connect('../data/UsersDB')
    cursor = connection.cursor()
    res = cursor.execute('''SELECT * FROM Users WHERE last_name LIKE ? AND first_name LIKE ? AND email LIKE ?;''',
                   (last_name, first_name, email))
    return res


def send_email(address: str) -> None:
    """this is total ChatGPT. I mostly understand what is happening here, but have no time right now for figuring out
    how to properly use SMTP servers
    this also means that this function was not tested"""
    smtp_server = 'smtp.example.com'
    smtp_port = 587
    smtp_username = 'your_username'
    smtp_password = 'your_password'

    with smtplib.SMTP(smtp_server, smtp_port) as server:
        server.starttls()
        server.login(smtp_username, smtp_password)

        message = MIMEMultipart()
        message['From'] = 'your_email@example.com'
        message['To'] = address
        message['Subject'] = 'Greetings and thank you'

        body = 'Dear user, thanks for joining our resource! We highly appreciate that.'
        message.attach(MIMEText(body, 'plain'))

        server.sendmail('your_email@example.com', address, message.as_string())


def register_new_user(user: User) -> None:
    send_email(user.email)


if __name__ == '__main__':
    birthday_1 = datetime.date(2001, 1, 8)
    user_1 = User('Nikolay', 'Ivanovich', 'Kamarov',
                  birthday=birthday_1, email='nikolaykomarov728@gmail.com')
    print([user_1.full_name, user_1.short_name, user_1.age])
    print(user_1)

    print(search_user_in_db(user_1.first_name, user_1.last_name, user_1.email))
