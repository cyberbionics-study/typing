from typing import List, Optional


class File:
    def __init__(self, name: str, directory: Optional['Directory']) -> None:
        self.name = name
        self.directory = directory


class Directory:
    def __init__(self, name: str, root: Optional['Directory'],
                 files: List[File] = None, sub_directories: List[Optional['Directory']] = None):
        self.name = name
        self.root = root
        self.files = files or []
        self.sub_directories = sub_directories or []

    def add_sub_directory(self, directory: Optional['Directory']) -> None:
        directory.root = self
        self.sub_directories.append(directory)

    def remove_sub_directory(self, directory: Optional['Directory']) -> None:
        directory.root = None
        self.sub_directories.remove(directory)

    def add_file(self, file: File) -> None:
        file.directory = self
        self.files.append(file)

    def remove_file(self, file: File) -> None:
        file.directory = None
        self.files.remove(file)
