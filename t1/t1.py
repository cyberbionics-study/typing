from typing import List


def some_func(some_list: List[int]) -> List[str] :
    res = []
    for elem in some_list:
        res.append(str(elem))
    return res
